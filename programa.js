var contexto;

function inicializar() {
    let lienzo = document.getElementById('miLienzo');
    lienzo.width = window.innerWidth;
    lienzo.height = window.innerHeight;
    contexto = lienzo.getContext('2d');
    contexto.beginPath();
    contexto.moveTo(0, (window.innerHeight / 2)-100);
    contexto.lineTo(window.innerWidth, (window.innerHeight / 2)-100);
    contexto.stroke();
    contexto.translate(0, window.innerHeight/2);
    contexto.scale(1, -1);

}


function dibujarCirculo(x, y, radio) {
    contexto.beginPath();
    contexto.arc(x, y, radio, 0, 2 * Math.PI, true);
    contexto.fill();
}


function graficaSeno(frecuencia) {
    inicializar()
    let x;
    let desplazamiento = 100;
    let amplitud = 100;
    for (x = 0; x < 360 * 4; x += (1 / frecuencia)){
        y = Math.sin(x * frecuencia * Math.PI / 180) * amplitud + desplazamiento;
        dibujarCirculo(x, y, 1);
    }
  


}

function graficaCoseno(frecuencia) {
    inicializar()
    let x;
    let desplazamiento = 100;
    let amplitud = 100;
    for (x = 0; x < 360 * 4; x += (1 / frecuencia)){
        y = Math.cos(x * frecuencia * Math.PI / 180) * amplitud + desplazamiento;
        dibujarCirculo(x, y, 1)
    }

}

